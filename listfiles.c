#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <grp.h>
#include <time.h>
#include <string.h>

#define BUFFER_SIZE (256)//standard buffer size for use with char arrays  and relating structs

/*
 - Function traverseFile : Enters gicen directory, prints out details on every regular file.
 - Printed detials are : permissions, links, owner, group, size, last modification time, name
 - Parameter cwd[] : char array string of directory to enter
 - Return VOID
*/
void traverseFile(char cwd[]);

int main (int argc, char *argv[])
{
	char cwd[BUFFER_SIZE];//store current working directory
	getcwd(cwd, BUFFER_SIZE); //get cwd

	traverseFile(cwd); //ENTER RECURSION

	return EXIT_SUCCESS;
}

void traverseFile(char cwd[])
{
	chdir(cwd);
	printf("\n%s\n",cwd); //print current directory
	DIR *dp = opendir(cwd); //open cwd
	struct dirent *entry; //for reading in the dir
	char buffer[BUFFER_SIZE]; //buffer to store file path

    //first print all files in directory
	while((entry = readdir(dp)) != NULL) //for each file in directory
	{
		struct stat sb; //stat struct to get info on files
		snprintf(buffer, BUFFER_SIZE, "%s/%s", cwd, entry->d_name); //get cwd into buffer

		stat(buffer, &sb); //fills stat struct with details on file

        if(S_ISREG(sb.st_mode)) 
        {
            printf("-");//print - for regular file
            //user permissions
            printf(sb.st_mode & S_IRUSR ? "r" : "-");
            printf(sb.st_mode & S_IWUSR ? "w" : "-");
            printf(sb.st_mode & S_IXUSR ? "x" : "-");
            //group permissions
            printf(sb.st_mode & S_IRGRP ? "r" : "-");
            printf(sb.st_mode & S_IWGRP ? "w" : "-");
            printf(sb.st_mode & S_IXGRP ? "x" : "-");
            //others permissions
            printf(sb.st_mode & S_IROTH ? "r" : "-");
            printf(sb.st_mode & S_IWOTH ? "w" : "-");
            printf(sb.st_mode & S_IXOTH ? "x" : "-");

            printf(" %d",sb.st_nlink); //print number of file links

            struct passwd *pwd = getpwuid(sb.st_uid); //get user information from file user id
            struct group *grp = getgrgid(sb.st_gid);//get group information for griup id

            printf(" %s",pwd->pw_name); //print user name
            printf(" %s",grp->gr_name); //print user group
            printf(" %d",sb.st_size); //print size in bytes

            char date[10]; //buffer for date
            char time[10]; //buffer for time
            strftime(date,10,"%b %d", localtime(&(sb.st_mtime))); //get formatted date
            strftime(time,10,"%R", localtime(&(sb.st_mtime))); //get formatted time
            printf(" %s %s",date,time); //print date and time of last modifcation
            printf(" %s\n",entry->d_name); //print file name
        }
	}

	closedir(dp); //close open dir for reading regular files
    
    DIR *dirdp = opendir(cwd); //open cwd again to read directories and enter recursion
    struct dirent *direntry; //for reading in the dir
	char dirbuffer[BUFFER_SIZE]; //buffer to store file path
    
    while((direntry = readdir(dirdp)) != NULL)
    {
        struct stat sb; //stat struct to get info on directories
        
        snprintf(dirbuffer, BUFFER_SIZE, "%s/%s", cwd, direntry->d_name); //get cwd into buffer
        stat(dirbuffer, &sb); //fills stat struct with details on file
        
         if(strcmp(direntry->d_name,".") != 0 && strcmp(direntry->d_name,"..") !=0) //check for own and upper dir
		{
             if(S_ISDIR(sb.st_mode)) //if normal directory
			{
					char newdir[BUFFER_SIZE]; //new directory to enter
					strcpy(newdir,cwd); //get copy of cwd
					char dir[BUFFER_SIZE] = "/";
					strcat(dir,direntry->d_name); //add file name
					strcat(newdir,dir); //append to new directory path
					traverseFile(newdir); //recursively run on directory 
			} 
        } 
    }
    closedir(dirdp);//close open dir for reading directories 
}
