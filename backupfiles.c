/*
 - Program : backupfiles.c
 - Auther : ajc17dru 100199337
 - Last modification : 03/13/2018
 - Description:
    Takes in input of a given date (as a string or within a file) and displays
    the details of all the files that come after that date. -t switch used to 
    specify a time, otherwise defualt time of 1970-01-01 00:00:00 used. -h 
    switch used to display help
*/
#define __USE_XOPEN
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <grp.h>
#include <time.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#define BUFFER_SIZE (256)//standard buffer size for use with char arrays and relating structs

/*
 - Function traverseFile : Enters given directory, prints out details on every regular file that was last modified after 
   the supplied date and time.
 - Printed detials are : permissions, links, owner, group, size, last modification time, name
 - Parameter cwd[] : char array string of directory to enter
 - Partameter time : struct tm that contains fields representing a date 
 - Return VOID
*/
void traverseFile(char cwd[], struct tm *time);

/*
 - Function printError : prints out error message if incorrect agrs supplied
 - Parameters NONE
 - Rerurn VOID
*/
void printError();

/*
 - Function printHelp : Prints help associated with the program
 - Parameters NONE
 - Rerurn VOID
*/
void printHelp();

/*
 - Function fillDefTime : Makes a time struct with the default time
 - Parameter NONE
 - Rerurn : struct tm with default time of 1970-01-01 00:00:00
*/
struct tm* fillDefTime();

int main (int argc, char *argv[])
{
	char cwd[BUFFER_SIZE];//store current working directory
	getcwd(cwd, BUFFER_SIZE); //get cwd 

	if (argc == 1)
	{
		printf("No supplied arguments. Use -h for help\n");
		return EXIT_SUCCESS;
	}
    if (strchr(argv[1],'-') != NULL) //check if beginning of switch char is present
    {
        if(strchr(argv[1],'h') != NULL) //print help, then check for other switches 
        {
            printHelp();
        }
        if (strchr(argv[1],'t') != NULL) //if t switch present
        {
            struct tm *time = (struct tm*)malloc(sizeof(struct tm));
            char input[BUFFER_SIZE];
            if (argc == 3) //only 1 argument supplied, must be directory with no time specified
            {
                time = fillDefTime(); //get default time
                strcpy(input,argv[argc-1]); //get copy of directory to work in

                int infd = open(input,O_RDONLY); //see if directory exists
                if (infd == -1)
                {
                    printf("Starting directory does not exist\n");
                    return EXIT_SUCCESS;
                }
                else //if directory exists, enter recursion with default time
                {
                    printf("Default time used\n");
                    traverseFile(input,time);
                    return EXIT_SUCCESS;
                }
            }
            else if (argc == 4) //if 2 arguments supplies (string/file and directory)
            {
                strcpy(input,argv[2]); //get copy of supplied argument
                if (input[0] == '/') //if file supplied
                {
                    int infd = open(input,O_RDONLY); //see if file exists by trying to open it
                    if (infd == -1) //-1 for if file does not exist
                    {
                        printf("File does not exist. Use -h for help.\n");
                        return EXIT_SUCCESS;
                    }
                    else //otherwise file exists
                    {
                        printf("File used\n");
                        struct stat sb; //stat struct for file stats
                        stat(input, &sb); // stat strut
                        strftime(input,BUFFER_SIZE,"%Y-%m-%d %H:%M:%S", localtime(&(sb.st_mtime)));
                        strptime(input,"%Y-%m-%d %H:%M:%S", time); //get last mod time of file
                        char directory[BUFFER_SIZE];
                        strcpy(directory,argv[argc-1]); //get directory to enter
                        traverseFile(directory,time);
                        return EXIT_SUCCESS; 
                    } 
                }
                else if(isdigit(input[0])) //if time string supplied
                {	//if strptime fails it returns null, therefore inforrect format
                    if (strptime(input,"%Y-%m-%d %H:%M:%S", time) == NULL)
                    { 
                        printf("Time in incorrect format. Use -h for help");
                        return EXIT_SUCCESS;
                    }
                    else //otherwise strptime was successful and filled tm struct
                    {
                        printf("Time string used\n");
                        char directory[BUFFER_SIZE];
                        strcpy(directory,argv[argc-1]); //get directory to enter
                        traverseFile(directory,time);
                        return EXIT_SUCCESS;
                    }
                }
                else //otherwise unrecognised arg pattern/format
                {
                    printError();
                    return EXIT_SUCCESS;
                }
            }
            else //otherwise unrecognised arg pattern/format
            {
                printError();
                return EXIT_SUCCESS;
            }
        }
    } 
	else //otherwise unrecognised arg pattern/format
	{
		printError();
		return EXIT_SUCCESS;
	}

	return EXIT_SUCCESS;
}

void traverseFile(char cwd[], struct tm *time)
{
	chdir(cwd);
	printf("\n%s\n",cwd); //print current directory
	DIR *dp = opendir(cwd); //open cwd
	struct dirent *entry; //for reading in the dir
	char buffer[BUFFER_SIZE]; //buffer to store file path

    //first print all files in directory
	while((entry = readdir(dp)) != NULL) //for each file in directory
	{
		struct stat sb; //stat struct to get info on files
		snprintf(buffer, BUFFER_SIZE, "%s/%s", cwd, entry->d_name); //get cwd into buffer

		stat(buffer, &sb); //fills stat struct with details on file 

        time_t argTime = mktime(time); //get time_t structs from file and argument
        time_t fileTime = sb.st_mtime;

        if (difftime(fileTime,argTime) > 0) //if files time comes after supplied time
        {
            if(S_ISREG(sb.st_mode)) 
            {
                printf("-");//print - for regular file
                //user permissions
                printf(sb.st_mode & S_IRUSR ? "r" : "-");
                printf(sb.st_mode & S_IWUSR ? "w" : "-");
                printf(sb.st_mode & S_IXUSR ? "x" : "-");
                //group permissions
                printf(sb.st_mode & S_IRGRP ? "r" : "-");
                printf(sb.st_mode & S_IWGRP ? "w" : "-");
                printf(sb.st_mode & S_IXGRP ? "x" : "-");
                //others permissions
                printf(sb.st_mode & S_IROTH ? "r" : "-");
                printf(sb.st_mode & S_IWOTH ? "w" : "-");
                printf(sb.st_mode & S_IXOTH ? "x" : "-");

                printf(" %d",sb.st_nlink); //print number of file links

                struct passwd *pwd = getpwuid(sb.st_uid); //get user information from file user id
                struct group *grp = getgrgid(sb.st_gid);//get group information for griup id

                printf(" %s",pwd->pw_name); //print user name
                printf(" %s",grp->gr_name); //print user group
                printf(" %d",sb.st_size); //print size in bytes

                char date[10]; //buffer for date
                char time[10]; //buffer for time
                strftime(date,10,"%b %d", localtime(&(sb.st_mtime))); //get formatted date
                strftime(time,10,"%R", localtime(&(sb.st_mtime))); //get formatted time
                printf(" %s %s",date,time); //print date and time of last modifcation
                printf(" %s\n",entry->d_name); //print file name
            }
        }
	}

	closedir(dp); //close open dir for reading regular files
    
    DIR *dirdp = opendir(cwd); //open cwd again to read directories and enter into recursion if needed
    struct dirent *direntry; //for reading in the dir
	char dirbuffer[BUFFER_SIZE]; //buffer to store file path
    
    while((direntry = readdir(dirdp)) != NULL) //while next file exists
    {
        struct stat sb; //stat struct to get info on directories
        
        snprintf(dirbuffer, BUFFER_SIZE, "%s/%s", cwd, direntry->d_name); //get cwd into buffer
        stat(dirbuffer, &sb); //fills stat struct with details on file
        
         if(strcmp(direntry->d_name,".") != 0 && strcmp(direntry->d_name,"..") !=0) //check for own and upper dir
		{
             if(S_ISDIR(sb.st_mode)) //if normal directory
			{
					char newdir[BUFFER_SIZE]; //new directory to enter
					strcpy(newdir,cwd); //get copy of cwd
					char dir[BUFFER_SIZE] = "/";
					strcat(dir,direntry->d_name); //add file name
					strcat(newdir,dir); //append to new directory path
					traverseFile(newdir,time); //recursively run on directory with supplied
			} 
        } 
    }
    closedir(dirdp);//close open dir for reading directories 
}

void printHelp()
{
	printf("Help for backupfiles\n");
    printf("Program will display all files that were modified after supplied date, either as a string or as a file path.\n");
	printf("Possible arguments: \n");
	printf("\t./backupfiles {-h,-t} {\"YYYY-MM-DD HH:MM:SS\",/filepath} {/directory}\n");
	printf("\t -h : display program help\n");
	printf("\t -h : to specify date files should appear from\n");
	printf("\t -t \"YYYY-MM-DD HH:MM:SS\" :  specified time to backup file from in string format\n");
	printf("\t -t /filepath : specified backup time contained in regular file at specified location\n");
	printf("\t /directory : specify where files should be displayed from\n");
	printf("\t Final argument must be filepath to desired starting directory\n");
	printf("\t If no date is specified, as argument or file, default of 1970-01-00 00:00:00 will be used\n");
	printf("\t Program will return details of files that come after specidied date\n"); 
}

void printError()
{
	printf("Invalid arguments. Use -h for help.\n");	
}

struct tm* fillDefTime()
{
	struct tm *defaultT; //make time struct to store default time
	//set default time to 1970-01-01 00:00:00
	defaultT->tm_sec = 0;
	defaultT->tm_min = 0;
	defaultT->tm_hour = 0;
	defaultT->tm_mday = 1;
	defaultT->tm_mon = 1;
	defaultT->tm_year = 1970 - 1900; //years since 1900 untill specified year (1970)

	return defaultT;
}	
